require 'rails_helper'

describe Color do 
   context "Número máximo de caracteres" do       
      it "50 de length para Color.name passa" do 
          c = Color.new
          c.name = 	'09876543210987654321098765432109876543210987654321'
          expect(c.save).to eq true
      end

      it "51 de length para Color.name NÃO passa" do 
          c = Color.new
          c.name = 	'098765432109876543210987654321098765432109876543210'
          begin
			  expect(c.save).to eq false
          rescue
              expect(1).to eq 1
          end    
      end

      it "0 de length para Color.name NÃO passa" do 
          c = Color.new
          
          begin
			  expect(c.save).to eq false
          rescue
              expect(1).to eq 1
          end    
      end

      it "1 de length para Color.name NÃO passa" do 
          c = Color.new
          c.name = 	'0'
		  expect(c.save).to eq true
      end

   end
end

