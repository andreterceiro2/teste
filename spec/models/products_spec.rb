require 'rails_helper'

describe Color do 
   context "Número máximo de caracteres" do       
      it "50 de length para Product.name passa" do 
          p = Product.new
          p.name = '09876543210987654321098765432109876543210987654321'
          p.description="teste"
          expect(p.save).to eq true
      end

      it "51 de length para Product.name NÃO passa" do 
          p = Product.new
          p.name = '098765432109876543210987654321098765432109876543210'
          p.description="teste"
          begin
			  expect(p.save).to eq false
          rescue
              expect(1).to eq 1
          end    
      end

      it "1 de length para Product.name passa" do 
          p = Product.new
          p.name = '0'
          p.description="teste"
          expect(p.save).to eq true
      end

      it "0 de length para Product.name NÃO passa" do 
          p = Product.new
          p.description="teste"
          begin
			  expect(p.save).to eq false
          rescue
              expect(1).to eq 1
          end    
      end

      it "1 de length para Product.description passa" do 
          p = Product.new
          p.name = 'teste'
          p.description="0"
          expect(p.save).to eq true
      end

      it "0 de length para Products.description NÃO passa" do 
          p = Product.new
          p.name="teste"
          begin
			  expect(p.save).to eq false
          rescue
              expect(1).to eq 1
          end    
      end
   end
end


