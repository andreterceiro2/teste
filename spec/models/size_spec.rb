require 'rails_helper'

describe Size do 
   context "Número máximo de caracteres" do       
      it "50 de length para Size.name passa" do 
          s = Size.new
          s.name = 	'09876543210987654321098765432109876543210987654321'
          expect(s.save).to eq true
      end

      it "51 de length para Size.name NÃO passa" do 
          s = Size.new
          s.name = 	'098765432109876543210987654321098765432109876543210'
          begin
			  expect(s.save).to eq false
          rescue
              expect(1).to eq 1
          end    
      end

      it "0 de length para Size.name NÃO passa" do 
          s = Size.new
          
          begin
			  expect(s.save).to eq false
          rescue
              expect(1).to eq 1
          end    
      end

      it "1 de length para Size.name passa" do 
          s = Size.new
          s.name = 	'0'
		  expect(s.save).to eq true
      end
   end
end

