Rails.application.routes.draw do
    resources :products
    resources :sizes
    resources :colors
    resources :avaliables

	root :to => "products#index"

    get '/colors/edit/:id', to: 'colors#edit', controller: 'colors'
    get '/colors/delete/:id', to: 'colors#delete'

    get '/sizes/edit/:id', to: 'sizes#edit', controller: 'sizes'
    get '/sizes/delete/:id', to: 'sizes#delete'

 	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html 
    post '/avaliables/create', to: 'avaliables#create'
    get '/avaliables/delete/:id', to: 'avaliables#delete' 
    
    get '/products/edit/:id', to: 'products#edit', controller: 'products'
    get '/products/delete/:id', to: 'products#delete' 
end
