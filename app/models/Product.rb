class Product < ActiveRecord::Base
    validates :name, length: {maximum: 50, minimum: 1}
    validates :description, length: {minimum: 1}
end

