json.extract! size, :id, :a, :b, :created_at, :updated_at
json.url size_url(size, format: :json)
