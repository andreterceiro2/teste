json.extract! color, :id, :a, :b, :created_at, :updated_at
json.url color_url(color, format: :json)
