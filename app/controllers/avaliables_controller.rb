class AvaliablesController < ApplicationController
	skip_before_action :verify_authenticity_token
    before_action :redirect_se_necessario

    def redirect_se_necessario
		if Color.count == 0 or Size.count == 0 or Product.count == 0 
           redirect_to 'http://127.0.0.1:3000/products', notice: "Please insert other models before"
        end
    end
    
    def index
		@avaliables = Avaliable.all
	end    
    
    def new        
        @colors = Color.all
        @sizes = Size.all
        @products = Product.all
        @errors=[]
        @avaliables = Avaliable.all
    end
    
    def create 
        @avaliable = Avaliable.new
        @avaliable.size_id = params[:size_id]
        @avaliable.color_id = params['color_id']        
        @avaliable.product_id = params['product_id']
		if @avaliable.save
			flash[:success] = "Register saved!"
			redirect_to "/avaliables"
		else
			flash[:alert] = "Register not saved!"
		end
    end
 
    def delete
		@avaliable = Avaliable.find(params[:id])
        @avaliable.destroy
        respond_to do |format|
            format.html { redirect_to controller: 'avaliables', notice: 'The register was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

    def avaliable_params
        params.require(:avaliables).permit(:color_id, :product_id, :size_id)
    end
end
